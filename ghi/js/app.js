function formatDate(dateString) {
    const date = new Date(dateString);
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const year = date.getFullYear().toString();
    return `${month}/${day}/${year}`;
  }

  function createCard(name, description, pictureUrl, startDate, endDate, location) {
    const formattedStartDate = formatDate(startDate);
    const formattedEndDate = formatDate(endDate);
    return `
      <div id="error-alert" class="alert alert-danger" style="display: none;"></div>
      <div class="col-md-4">
        <div class="card">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-location" mb-2 text-muted" style="color: gray;">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
            <p class="card-date-starts">${formattedStartDate}-${formattedEndDate}</p>
          </div>
        </div>
      </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // figure out what to do when the response is bad
            console.log(response);
        } else {
            const data = await response.json();

            for (let i = 0; i < data.conferences.length; i++) {
                const conference = data.conferences[i];

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                } else {
                    const errorAlert = document.getElementById('error-alert');
                    errorAlert.textContent = 'An error occurred while fetching data. Please try again later.';
                    errorAlert.style.display = 'block';
                }
            }

        }
    } catch (e) {
        // Figure out what to do if an error is raised
        const errorAlert = document.getElementById('error-alert');
        errorAlert.textContent = 'An error occurred while fetching data. Please try again later.';
        errorAlert.style.display = 'block';
    }
});
