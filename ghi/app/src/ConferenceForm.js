import React, { useEffect, useState } from 'react';

function ConferenceForm( ) {
  const [name, setName] = useState('');
  const [starts, setStartDate] = useState('');
  const [ends, setEndDate] = useState('');
  const [description, setDescription] = useState('');
  const [presentations, setPresentation] = useState('');
  const [attendees, setAttendees] = useState('');
  const [locations, setLocation] = useState([]);
  const [selectedLocation, setSelectedLocation] = useState('');

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleStartDateChange = (event) => {
    const value = event.target.value;
    setStartDate(value);
  };

  const handleEndDateChange = (event) => {
    const value = event.target.value;
    setEndDate(value);
  };

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handlePresentationChange = (event) => {
    const value = event.target.value;
    setPresentation(value);
  };

  const handleAttendeesChange = (event) => {
    const value = event.target.value;
    setAttendees(value);
  };

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setSelectedLocation(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = presentations;
    data.max_attendees = attendees;
    data.location = selectedLocation;
    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setName('');
      setStartDate('');
      setEndDate('');
      setDescription('');
      setPresentation('');
      setAttendees('');
      setLocation('');
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setLocation(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartDateChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndDateChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div>
              <label htmlFor="description" className="form-label">Description</label>
              <textarea onChange={handleDescriptionChange} value={description} required type="text" name="description" id="description" className="form-control" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePresentationChange} value={presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAttendeesChange} value={attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} value={selectedLocation} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.locations?.map((location) => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
